class Triangle:
    def __init__(self, vertices):
        self.vertices = vertices

    def calculate_area(self):
        # We will use vertices in (x, y, z) format
        # Calculating area using cross product of two sides of the triangle
        a = self.vertices[1][0] - self.vertices[0][0]
        b = self.vertices[1][1] - self.vertices[0][1]
        c = self.vertices[1][2] - self.vertices[0][2]

        d = self.vertices[2][0] - self.vertices[0][0]
        e = self.vertices[2][1] - self.vertices[0][1]
        f = self.vertices[2][2] - self.vertices[0][2]

        area = 0.5 * abs(a * e - b * d + c * e - f * c)
        return area

# Old logic that we like to keep
# def classify_triangles(mesh_file):
#     triangles = []

#     # Read mesh file and parse triangles
#     with open(mesh_file, 'r') as file:
#         for line in file:
#             if line.startswith("f "):  # Assuming faces are defined with 'f' in the mesh file
#                 vertices = []
#                 for vertex in line.split()[1:]:
#                     vertices.append(tuple(map(float, vertex.split('/')))) 
#                 triangles.append(Triangle(vertices))

#     # Calculate areas and classify triangles
#     for triangle in triangles:
#         area = triangle.calculate_area()
#         if area < threshold_column:
#             print("Column:", area)
#             # Assign to 'Column' class
#         elif threshold_column <= area < threshold_window:
#             print("Window:", area)
#             # Assign to 'Window' class
#         else:
#             print("Wall:", area)
#             # Assign to 'Wall' class
    

def classify_triangles(mesh_file, threshold_column, threshold_window):
    segments = {'Column': [], 'Window': [], 'Wall': []}

    # Read mesh file and parse triangles
    with open(mesh_file, 'r') as file:
        for line in file:
            if line.startswith("f "):  # faces are defined with 'f' in the mesh file
                vertices = []
                for vertex in line.split()[1:]:
                    vertices.append(tuple(map(float, vertex.split('/'))))  # vertices are in (x, y, z) format
                triangle = Triangle(vertices, None)
                area = triangle.calculate_area()
                if area < threshold_column:
                    segments['Column'].append(triangle)
                elif threshold_column <= area < threshold_window:
                    segments['Window'].append(triangle)
                else:
                    segments['Wall'].append(triangle)


    # Merge segments into a single mesh
    merged_vertices = []
    merged_faces = []
    vertex_index = 1
    for segment_name, triangles in segments.items():
        for triangle in triangles:
            for vertex in triangle.vertices:
                merged_vertices.append(vertex)
            for i in range(3):
                merged_faces.append((vertex_index, vertex_index + 1, vertex_index + 2))
                vertex_index += 3

    # Write merged mesh to file
    write_merged_mesh("merged_mesh.obj", merged_vertices, merged_faces)


def write_merged_mesh(filename, vertices, faces):
    with open(filename, 'w') as file:
        for v in vertices:
            file.write("v {} {} {}\n".format(v[0], v[1], v[2]))  # Write vertex
        for f in faces:
            file.write("f {} {} {}\n".format(f[0], f[1], f[2]))  # Write face


# Define thresholds for triangle sizes
threshold_column = 1.0  # Threshold values based on the mesh and its units
threshold_window = 8.0
threshold_wall = 20.0


# Replace "SodaMesh.obj" with actual mesh file
classify_triangles("SodaMesh.obj", threshold_column, threshold_window) 
